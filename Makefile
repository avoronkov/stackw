.PHONY: all clean

all:
	go build -o stackw
	./stackw example.txt example.wat
	wat2wasm example.wat -o example.wasm
	wasmtime example.wasm

clean:
	rm -f example.wat example.wasm stackw
