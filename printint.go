package main

const fnReverse = `
  (func $reverse (param i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    local.set 2
    i32.const 32
    local.set 3
    local.get 2
    local.get 3
    i32.sub
    local.set 4
    i32.const 0
    local.set 5
    local.get 4
    local.get 0
    i32.store offset=28
    local.get 4
    local.get 1
    i32.store offset=24
    local.get 4
    local.get 5
    i32.store offset=20
    local.get 4
    i32.load offset=24
    local.set 6
    i32.const 1
    local.set 7
    local.get 6
    local.get 7
    i32.sub
    local.set 8
    local.get 4
    local.get 8
    i32.store offset=16
    block  ;; label = @1
      loop  ;; label = @2
        local.get 4
        i32.load offset=20
        local.set 9
        local.get 4
        i32.load offset=16
        local.set 10
        local.get 9
        local.set 11
        local.get 10
        local.set 12
        local.get 11
        local.get 12
        i32.lt_s
        local.set 13
        i32.const 1
        local.set 14
        local.get 13
        local.get 14
        i32.and
        local.set 15
        local.get 15
        i32.eqz
        br_if 1 (;@1;)
        local.get 4
        i32.load offset=28
        local.set 16
        local.get 4
        i32.load offset=20
        local.set 17
        local.get 16
        local.get 17
        i32.add
        local.set 18
        local.get 18
        i32.load8_u
        local.set 19
        local.get 4
        local.get 19
        i32.store8 offset=15
        local.get 4
        i32.load offset=28
        local.set 20
        local.get 4
        i32.load offset=16
        local.set 21
        local.get 20
        local.get 21
        i32.add
        local.set 22
        local.get 22
        i32.load8_u
        local.set 23
        local.get 4
        i32.load offset=28
        local.set 24
        local.get 4
        i32.load offset=20
        local.set 25
        local.get 24
        local.get 25
        i32.add
        local.set 26
        local.get 26
        local.get 23
        i32.store8
        local.get 4
        i32.load8_u offset=15
        local.set 27
        local.get 4
        i32.load offset=28
        local.set 28
        local.get 4
        i32.load offset=16
        local.set 29
        local.get 28
        local.get 29
        i32.add
        local.set 30
        local.get 30
        local.get 27
        i32.store8
        local.get 4
        i32.load offset=20
        local.set 31
        i32.const 1
        local.set 32
        local.get 31
        local.get 32
        i32.add
        local.set 33
        local.get 4
        local.get 33
        i32.store offset=20
        local.get 4
        i32.load offset=16
        local.set 34
        i32.const -1
        local.set 35
        local.get 34
        local.get 35
        i32.add
        local.set 36
        local.get 4
        local.get 36
        i32.store offset=16
        br 0 (;@2;)
      end
    end
    return)
`
const fnItoa = `
  (func $itoa (param i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    local.set 2
    i32.const 16
    local.set 3
    local.get 2
    local.get 3
    i32.sub
    local.set 4
    local.get 4
    global.set 0
    i32.const 0
    local.set 5
    local.get 4
    local.get 0
    i32.store offset=12
    local.get 4
    local.get 1
    i32.store offset=8
    local.get 4
    i32.load offset=12
    local.set 6
    local.get 4
    local.get 6
    i32.store
    local.get 6
    local.set 7
    local.get 5
    local.set 8
    local.get 7
    local.get 8
    i32.lt_s
    local.set 9
    i32.const 1
    local.set 10
    local.get 9
    local.get 10
    i32.and
    local.set 11
    block  ;; label = @1
      local.get 11
      i32.eqz
      br_if 0 (;@1;)
      i32.const 0
      local.set 12
      local.get 4
      i32.load offset=12
      local.set 13
      local.get 12
      local.get 13
      i32.sub
      local.set 14
      local.get 4
      local.get 14
      i32.store offset=12
    end
    i32.const 0
    local.set 15
    local.get 4
    local.get 15
    i32.store offset=4
    loop  ;; label = @1
      local.get 4
      i32.load offset=12
      local.set 16
      i32.const 10
      local.set 17
      local.get 16
      local.get 17
      i32.rem_s
      local.set 18
      i32.const 48
      local.set 19
      local.get 18
      local.get 19
      i32.add
      local.set 20
      local.get 4
      i32.load offset=8
      local.set 21
      local.get 4
      i32.load offset=4
      local.set 22
      i32.const 1
      local.set 23
      local.get 22
      local.get 23
      i32.add
      local.set 24
      local.get 4
      local.get 24
      i32.store offset=4
      local.get 21
      local.get 22
      i32.add
      local.set 25
      local.get 25
      local.get 20
      i32.store8
      i32.const 0
      local.set 26
      local.get 4
      i32.load offset=12
      local.set 27
      i32.const 10
      local.set 28
      local.get 27
      local.get 28
      i32.div_s
      local.set 29
      local.get 4
      local.get 29
      i32.store offset=12
      local.get 29
      local.set 30
      local.get 26
      local.set 31
      local.get 30
      local.get 31
      i32.gt_s
      local.set 32
      i32.const 1
      local.set 33
      local.get 32
      local.get 33
      i32.and
      local.set 34
      local.get 34
      br_if 0 (;@1;)
    end
    i32.const 0
    local.set 35
    local.get 4
    i32.load
    local.set 36
    local.get 36
    local.set 37
    local.get 35
    local.set 38
    local.get 37
    local.get 38
    i32.lt_s
    local.set 39
    i32.const 1
    local.set 40
    local.get 39
    local.get 40
    i32.and
    local.set 41
    block  ;; label = @1
      local.get 41
      i32.eqz
      br_if 0 (;@1;)
      i32.const 45
      local.set 42
      local.get 4
      i32.load offset=8
      local.set 43
      local.get 4
      i32.load offset=4
      local.set 44
      i32.const 1
      local.set 45
      local.get 44
      local.get 45
      i32.add
      local.set 46
      local.get 4
      local.get 46
      i32.store offset=4
      local.get 43
      local.get 44
      i32.add
      local.set 47
      local.get 47
      local.get 42
      i32.store8
    end
    i32.const 10
    local.set 48
    local.get 4
    i32.load offset=8
    local.set 49
    local.get 4
    i32.load offset=4
    local.set 50
    local.get 49
    local.get 50
    call $reverse
    local.get 4
    i32.load offset=8
    local.set 51
    local.get 4
    i32.load offset=4
    local.set 52
    i32.const 1
    local.set 53
    local.get 52
    local.get 53
    i32.add
    local.set 54
    local.get 4
    local.get 54
    i32.store offset=4
    local.get 51
    local.get 52
    i32.add
    local.set 55
    local.get 55
    local.get 48
    i32.store8
    local.get 4
    i32.load offset=4
    local.set 56
    i32.const 16
    local.set 57
    local.get 4
    local.get 57
    i32.add
    local.set 58
    local.get 58
    global.set 0
    local.get 56
    return)
`

const fnWrite = `
  (func $write (param i32 i32 i32) (result i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 3
    global.set 0
    local.get 3
    local.get 2
    i32.store offset=12
    local.get 3
    local.get 1
    i32.store offset=8
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        local.get 3
        i32.const 8
        i32.add
        i32.const 1
        local.get 3
        i32.const 4
        i32.add
        call $__wasi_fd_write
        local.tee 0
        i32.eqz
        br_if 0 (;@2;)
        i32.const 0
        i32.const 8
        local.get 0
        local.get 0
        i32.const 76
        i32.eq
        select
        i32.store offset=1024
        i32.const -1
        local.set 0
        br 1 (;@1;)
      end
      local.get 3
      i32.load offset=4
      local.set 0
    end
    local.get 3
    i32.const 16
    i32.add
    global.set 0
    local.get 0)
`

const fnPrintInt = `
  (func $printInt (param i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    local.set 1
    i32.const 160
    local.set 2
    local.get 1
    local.get 2
    i32.sub
    local.set 3
    local.get 3
    global.set 0
    i32.const 1
    local.set 4
    i32.const 16
    local.set 5
    local.get 3
    local.get 5
    i32.add
    local.set 6
    local.get 6
    local.set 7
    local.get 3
    local.get 0
    i32.store offset=156
    local.get 3
    i32.load offset=156
    local.set 8
    local.get 8
    local.get 7
    call $itoa
    local.set 9
    local.get 3
    local.get 9
    i32.store offset=12
    local.get 3
    i32.load offset=12
    local.set 10
    local.get 4
    local.get 7
    local.get 10
    call $write
    drop
    local.get 3
    i32.load offset=156
    local.set 11
    i32.const 160
    local.set 12
    local.get 3
    local.get 12
    i32.add
    local.set 13
    local.get 13
    global.set 0
    local.get 11
    return)
`
