#include <unistd.h>

/* reverse:  переворачиваем строку s на месте */
void reverse(char s[], int len)
{
	int i, j;
	char c;

	for (i = 0, j = len-1; i<j; i++, j--) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

/* itoa:  конвертируем n в символы в s */
 int itoa(int n, char s[])
 {
     int i, sign;
 
     if ((sign = n) < 0)  /* записываем знак */
         n = -n;          /* делаем n положительным числом */
     i = 0;
     do {       /* генерируем цифры в обратном порядке */
         s[i++] = n % 10 + '0';   /* берем следующую цифру */
     } while ((n /= 10) > 0);     /* удаляем */
     if (sign < 0)
         s[i++] = '-';
     reverse(s, i);
	 s[i++] = '\n';
	 return i;
 }


int printInt(int x) {
	char buf[128];
	int l = itoa(x, buf);
	write(1, buf, l);
	return x;
}

int main() {
	int x = 4731;
	printInt(x);
}
