package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

const (
	moduleHeader = `(module`
	moduleFooter = `)`

	declareTypes = `
  (type (;0;) (func (param i32)))
  (type (;1;) (func (param i32 i32 i32 i32) (result i32)))
`

	wasiImports = `
  (import "wasi_snapshot_preview1" "proc_exit" (func $__wasi_proc_exit (type 0)))
  (import "wasi_snapshot_preview1" "fd_write" (func $__wasi_fd_write (type 1)))
`

	memoryExport = `
  (memory 2)
  (export "memory" (memory 0))
`
	globals = `  (global (;0;) (mut i32) (i32.const 66576))`

	mainHeader = `  (func $main`
	mainFooter = `  return)`

	exportMain = `  (export "_start" (func $main))`
)

var ops = map[string]string{
	"+":     "i32.add",
	"*":     "i32.mul",
	"-":     "i32.sub",
	"print": "call $printInt",
	"drop":  "drop",
}

func Translate(in io.Reader, out io.Writer) error {
	fmt.Fprintf(out, "%v\n", moduleHeader)
	fmt.Fprintf(out, "%v\n", declareTypes)
	fmt.Fprintf(out, "%v\n", wasiImports)
	fmt.Fprintf(out, "%v\n", memoryExport)
	fmt.Fprintf(out, "%v\n", globals)
	fmt.Fprintf(out, "%v\n", fnReverse)
	fmt.Fprintf(out, "%v\n", fnItoa)
	fmt.Fprintf(out, "%v\n", fnWrite)
	fmt.Fprintf(out, "%v\n", fnPrintInt)

	fmt.Fprintf(out, "%v\n", mainHeader)

	sc := bufio.NewScanner(in)
	sc.Split(bufio.ScanLines)
	for sc.Scan() {
		line := sc.Text()
		tokens := strings.Fields(line)
		if err := addTokens(tokens, out); err != nil {
			return err
		}
	}
	if err := sc.Err(); err != nil {
		return err
	}

	fmt.Fprintf(out, "%v\n", mainFooter)
	fmt.Fprintf(out, "%v\n", exportMain)
	fmt.Fprintf(out, "%v\n", moduleFooter)
	return nil
}

func addTokens(tokens []string, out io.Writer) error {
	for _, token := range tokens {
		// check if number
		n, err := strconv.Atoi(token)
		if err == nil {
			fmt.Fprintf(out, "    (i32.const %v)\n", n)
			continue
		}
		if op, ok := ops[token]; ok {
			fmt.Fprintf(out, "    %v\n", op)
		} else {
			return fmt.Errorf("Unknown token: %v", token)
		}
	}
	return nil
}

func main() {
	var in io.ReadCloser
	var out io.WriteCloser
	var err error
	if len(os.Args) < 2 {
		in = ioutil.NopCloser(os.Stdin)
	} else {
		in, err = os.Open(os.Args[1])
		if err != nil {
			log.Fatal(err)
		}
	}
	defer in.Close()

	if len(os.Args) < 3 {
		out = os.Stdout
	} else {
		out, err = os.OpenFile(os.Args[2], os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	}
	defer out.Close()

	err = Translate(in, out)
	if err != nil {
		log.Fatal(err)
	}
}
